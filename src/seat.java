public class seat {
	
	private double [][] arrSeatAvailable;
	
	public seat()
	{
		arrSeatAvailable = data.ticketPrices;
	}
	
	public boolean chkAvailable(int i, int j)
	{		
		if(arrSeatAvailable[i][j] > 0)
			return true;
		else
			return false;
	}
	public String getSeatName(int row, int col)
	{
		String str="";
		str = (char)(65+row-1)+ (""+col);		
		return str;
	}
	
	public double[][] getSeatAvailable()
	{
		return arrSeatAvailable;
	}
	
	public int getRow()
	{
		return data.ROW;		
	}
	
	public int getCol()
	{
		return data.COL;
	}
}
