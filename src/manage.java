

public class manage {
	
	seat seat;
	double [][] arrSeatAvailable;
	
	public manage()
	{
		seat = new seat();
		arrSeatAvailable = seat.getSeatAvailable();
	}
	
	public seat getSeat()
	{
		return seat;
	}
	
	public void buySeat(int row, int col) 
	{
		arrSeatAvailable[row][col] = 0;
	}
	
	public int getRow()
	{
		return data.ROW;		
	}
	
	public int getCol()
	{
		return data.COL;
	}
	
	public double[][] getSeatAvailable()
	{
		return arrSeatAvailable;
	}
}
